<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestimonialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('testimonials')->insert([
            'company_name' => 'Company A',
            'content' => 'Testimonial content for Company A.',
        ]);

        DB::table('testimonials')->insert([
            'company_name' => 'Company B',
            'content' => 'Testimonial content for Company B.',
        ]);

        // Add more data if needed
    }

}
