<?php
namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    
    public function index()
{
    $testimonials = Testimonial::all();

    $formattedTestimonials = $testimonials->map(function ($testimonial) {
        return [
            'type' => 'testimonial',
            'id' => $testimonial->id,
            'attributes' => $testimonial,
        ];
    });

    return response()->json(['data' => $formattedTestimonials]);
}

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'company_name' => 'required',
            'admin' => 'required',
            'content' => 'required',
            'icon' => 'required|image|mimes:jpeg,png,jpg,gif', 
            'rating_stars' => 'required',
        ]);
        $uploadedFile = $request->file('icon');
    
        $fileName = uniqid() . '.' . $uploadedFile->getClientOriginalExtension();
    
        $uploadedFile->move(public_path('testimonial_icons'), $fileName);
    
        $testimonial = Testimonial::create([
            'company_name' => $validatedData['company_name'],
            'admin' => $validatedData['admin'],
            'content' => $validatedData['content'],
            'icon' => 'testimonial_icons/' . $fileName, 
            'rating_stars' => $validatedData['rating_stars'],
        ]);
    
        return response()->json([
            'message' => 'Testimonial created successfully',
            'data' => [
                'type' => 'testimonial',
                'id' => $testimonial->id,
                'attributes' => $testimonial,
            ],
        ], 201);
    }
    
    
    


    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'company_name' => 'required',
            'admin' => 'required',
            'content' => 'required',
            'rating_stars' => 'required',
        ]);
    
        $testimonial = Testimonial::findOrFail($id);
        $testimonial->update([
            'company_name' => $validatedData['company_name'],
            'admin' => $validatedData['admin'],
            'content' => $validatedData['content'],
            'rating_stars' => $validatedData['rating_stars'],
        ]);
        if ($request->hasFile('icon')) {
            $iconPath = $request->file('icon')->store('testimonial_icons', 'public');
            $testimonial->icon = $iconPath;
            $testimonial->save();
        }
    
        return response()->json(['message' => 'Testimonial updated successfully', 'testimonial' => $testimonial], 200);
    }
    

    public function destroy($id)
{
    $testimonial = Testimonial::findOrFail($id);
    $testimonial->delete();

    return response()->json(['message' => 'Testimonial deleted successfully'], 200);
}

}

