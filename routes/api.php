<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestimonialController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::get('/testimonials', [TestimonialController::class, 'index']);
// Route::post('testimonials', 'TestimonialController@store');
Route::ApiResource('testimonials', TestimonialController::class);
// Route::put('testimonials/{id}', [TestimonialController::class, 'update']);
// Route::delete('testimonials/{id}', [TestimonialController::class, 'destroy']);




